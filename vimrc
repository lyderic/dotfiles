"Lyderic Landry's ViM configuration file

"Required
set nocompatible

"My <leader> key (overwrites \)
let mapleader = '-'

"We always want UTF-8 whenever possible
set encoding=utf-8

"Alternatives to [ESC] in insert mode
inoremap jj <esc>

"[ESC] with simpler saving in insert mode
inoremap jk <esc>:w<cr>

"Simply hitting '=' in normal mode saves the buffer
nnoremap = :w<cr>

"Hit '+' in normal mode to save and quit
nnoremap + :wq<cr>

"Show italics and bolds nicely in markdown
set conceallevel=3

"Save and execute current buffer as script
command! Run normal <esc>:w<cr> :!./%<cr>

" In code mode, 'Q' simply shows the registers
nnoremap Q :registers<cr>

"Jump to the last position when reopening a file
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

"Display a statusline even if there is only one widow open
set laststatus=2

"Set up a default statusline
set statusline=%f\ %h%w%m%r\ %=%(%l,%c%V\ %=\ %P%)

"Wrap to the next line without breaking the words
set wrap
set linebreak

"In case of joining lines, no double space after a dot
set nojoinspaces
"Display as much characters that can fit on the line, independently of the
"frame width
set textwidth=0

"Autoindent when return to the next line (useful in programming)
set autoindent

"Helps indenting when writing code
set smartindent

"Autosave current buffer when it is changed to another buffer
set autowrite

"Show line numbers
set number

"Switch line numbering on/off
nnoremap _ :set number!<cr>

"Move easily from one window to another by hitting TAB
nnoremap <tab> <c-w>w

"Scrolling down one page by pressing of [Space]
nnoremap <Space> <PageDown>

"Scrolling up one page by pressing of [Backspace]
map <BS> <PageUp>

"Always show at least one line above/below the cursor
set scrolloff=5

"Default tab for 2
set ts=2

"We now indent with TABs as default as it makes Golang and Makefile happy
"However, YAML doesn't allow tabs as indentation
autocmd FileType yaml setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
autocmd! BufWritePost *.yaml,*yml silent! retab | redraw!

"Default backspace like normal
set bs=2

"Some option desactivated by default (remove the no).
set nobackup
set nohlsearch

"Start searching before pressing enter
set incsearch

"Show the position of the cursor.
set ruler

"Show matching parenthese.
set showmatch

"Switch the bells (visual or acoustic) off
set t_vb=
set novisualbell
set noerrorbells

"Show all changes
set report=0

"Create new window below current one.
set splitbelow

"Show (partial) command keys in the status line
set showcmd

"Supress intro message
set shortmess=I

"Define set list options
set nolist

"Enhanced commandline completion
set wildmenu

"Syntax on by default
syntax on

"Ignore case in searches
set ignorecase

"Set width for autoincrement
set shiftwidth=2

"Don't show @s if the last line does not fit
set display=lastline

"Remap j and k to be usable on long lines
nnoremap j gj
nnoremap k gk

"Command to change the directory to the directory of the current file
command! CurrentDir normal :cd %:h<cr>:pwd<cr>

"Deal with background
set bg=dark
command! Dark normal <esc>:set bg=dark<cr>
command! Light normal <esc>:set bg=light<cr>

"GOLANG STUFF
autocmd! BufNewFile,BufRead *.go set autoread
"run goimports
nnoremap <leader>i :!goimports -w %<cr>
"run goimports every time a go buffer is saved
autocmd! BufWritePost *.go silent! execute "!goimports -w %" | redraw!
"Some useful expands for go programming
iabbr iferr if err != nil { panic(err) }
iabbr errnn ; err != nil {  return}
iabbr cmdo cmd.Stdout, cmd.Stderr = os.Stdout, os.Stderr
iabbr cmdi cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
iabbr gtools . "github.com/lyderic/tools"

"We want .tex files to be always or type latex!
let g:tex_flavor='latex'

"Use as strong encryption as possible
set cryptmethod=blowfish2

"Open terminal to the right (vim 8.1+ required!)
command! VTerm :vertical rightbelow terminal

"Word completion with [TAB], except when at begining of line
"Doesn't work with accented characters :-(
function! SmartTabCompletion()
   let col = col('.') - 1
   if !col || getline('.')[col - 1] !~ '\k'
      return "\<tab>"
   else
      return "\<c-n>"
   endif
endfunction
inoremap <tab> <c-r>=SmartTabCompletion()<cr>
