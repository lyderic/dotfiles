#!/bin/bash

FILES="bigbang inputrc tmux.conf vimrc"

main() {
	case $1 in
		install) install ;;
		pack) pack ;;
		*) echo "Usage: $(basename $0) install | pack" ;;
	esac
}

install() {
	for file in ${FILES}
	do
		cp -uv ${file} "${HOME}/.${file}"
	done
	if ! grep bigbang "${HOME}/.bashrc" ; then
		echo '. ${HOME}/.bigbang' >> ~/.bashrc
	fi
}

pack() {
	for file in ${FILES}
	do
		cp -uv "${HOME}/.${file}" ${file}
	done
}

main ${@}
